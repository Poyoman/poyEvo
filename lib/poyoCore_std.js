var pc = new function() {
	var that = this;

	this.mouse = {
		x: 0,
		y: 0,
		trackerStarted: false
	};

	this.gebid = function(id) {
		return document.getElementById(id);
	};

	this.reloadMe = function() {
		window.location.reload(true);
	};

	this.goPage = function(url) {
		window.location.href = url;
	};

	this.goPage_fct = function(url) {
		return function() {
			that.goPage(url);
		};
	};

	this.applySeveralDatas = function(tDatas) {
		for (var key in tDatas) {
			pc.gebid(key).innerHTML = tDatas[key];
		}
	};

	// targetProperty examples : "style.display"
	this.setAtribute = function(targetObject, targetProperty, value) {
		var propertyPath = targetProperty.split('.');

		for (var i=0; i<propertyPath.length -1; ++i) {
			targetObject = targetObject[propertyPath[i]];
		}

		targetObject[propertyPath[propertyPath.length-1]] = value;
	};

	this.getAtribute = function( targetObject, targetProperty) {
		var propertyPath = targetProperty.split('.');

		for (var i=0; i<propertyPath.length -1; ++i) {
			targetObject = targetObject[propertyPath[i]];
		}

		return targetObject[propertyPath[propertyPath.length-1]];
	};

	this.trackMouse = function() {
		if (!that.mouse.trackerStarted) {
			that.mouse.trackerStarted = true;

			document.addEventListener('mousemove', function(e) {
				that.mouse.x = e.clientX || e.pageX;
				that.mouse.y = e.clientY || e.pageY;
			}, false);
		}
	};

	this.wHeight = function() {
		return window.innerHeight;
	};

	this.wWidth = function() {
		return window.innerWidth;
	};

	// return real dimensions of content of elt
	this.getInnerDim = function(elt) {
		var dims = {
			'x':elt.offsetLeft,
			'y':elt.offsetTop,
			'w':elt.offsetWidth,
			'h':elt.offsetHeight
		};

		if (elt.children != undefined)
			for (var i=0; i<elt.children.length; ++i) {
				var uDims = that.getInnerDim( elt.children[i]);

				dims.w = Math.max( dims.w, uDims.w + dims.x);
				dims.h = Math.max( dims.h, uDims.h + dims.y);
			}

		return dims;
	};

	// return position of object relative to browser windows
	this.getAbsPos = function(elt) {
		var px = 0;
		var py = 0;
		do {
			px += elt.offsetLeft || 0;
			py += elt.offsetTop || 0;
			elt = elt.offsetParent;
		} while(elt);

		return {
			'x': px,
			'y': py
		};
	};
};
