var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var ressources = {
	poyEvoFiles: () => (
		gulp.src(['lib/poyoCore_std.js', 'src/poyEvo2.js'])
	),

	poyEvo_debugFiles: () => (
		gulp.src('src/poyEvo2_debug.js')
	)
};

gulp.task('js', () => {
	return ressources.poyEvoFiles()
		.pipe(concat('poyEvo.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/'));
});

gulp.task('js-debug', () => {
	return ressources.poyEvo_debugFiles()
		.pipe(concat('poyEvo_debug.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/'));
});

gulp.task('js-nomin', () => {
	return ressources.poyEvoFiles()
		.pipe(concat('poyEvo.js'))
		.pipe(gulp.dest('build/'));
});

gulp.task('js-nomin-debug', () => {
	return ressources.poyEvo_debugFiles()
		.pipe(concat('poyEvo_debug.js'))
		.pipe(gulp.dest('build/'));
});

gulp.task('default', ['js', 'js-debug', 'js-nomin', 'js-nomin-debug']);

